import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

const API = 'https://dev.fitgage.us/api/v1/';
const header = {'Content-Type': 'application/json; charset=UTF-8'};

class AppService {
  List activitiesData;

  signIn(String username, String password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    Map data = {'username': username.trim(), 'password': password.trim()};
    var jsonResponse;
    jsonResponse = null;

    var response = await http.post(API + 'user/login/',
        headers: header, body: jsonEncode(data));

    print(response.body);

    if (response.statusCode == 200 || response.statusCode == 201) {
      jsonResponse = jsonDecode(response.body);

      if (jsonResponse != null) {
        sharedPreferences.setString("token", jsonResponse['access']);
        return response.statusCode;
      }
    } else if (response.statusCode == 401) {
      return response.body;
    } else {
      throw Exception('Failed to load post');
    }
  }

  registry(String email, password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map data = {
      'username': email.trim(),
      'email': email.trim(),
      'password': password.trim()
    };
    var jsonResponse;
    jsonResponse = null;

    var response =
        await http.post(API + 'user/', headers: header, body: jsonEncode(data));

    if (response.statusCode == 200 || response.statusCode == 201) {
      jsonResponse = jsonDecode(response.body);

      if (jsonResponse != null) {
        sharedPreferences.setString("token", jsonResponse['access']);
        return response.statusCode;
      }
    } else if (response.statusCode == 400) {
      return response.statusCode;
    } else {
      return throw Exception('Failed to load post');
    }
  }

  registerData(String name, lastname, birthDate, location) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    Map data = {
      'name': name.trim(),
      'lastname': lastname.trim(),
      'birth_date': birthDate.trim(),
      'location': location,
      'location_coords': location.position,
    };

    final value = sharedPreferences.get('token');

    var jsonResponse;
    jsonResponse = null;

    var response = await http.post(API + 'user/profile/',
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ' + value
        },
        body: jsonEncode(data));

    print(response.body);

    if (response.statusCode == 200 || response.statusCode == 201) {
      jsonResponse = jsonDecode(response.body);

      sharedPreferences.setString("name", jsonResponse["name"]);
      sharedPreferences.setString("lastname", jsonResponse["lastname"]);
      sharedPreferences.setString("birth_date", jsonResponse["birth_date"]);

      return response.statusCode;
    } else if (response.statusCode == 400) {
      return response.statusCode;
    } else {
      throw Exception('Failed to load post');
    }
  }

  forgotPassword(String email) async {
    Map data = {'email': email.trim()};
    var response = await http.post(API + 'api/v1/user/forgot_password.php',
        body: jsonEncode(data));

    if (response.statusCode == 200 || response.statusCode == 201) {
      json.decode(response.body);
    } else {}
  }

  registerTeacher(String businessName, businessNumber, identityNumber, _points,
      pdto, _blockCheck) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    Map address = {
      'address': _points.address,
      'latitude': _points.latitude,
      'longitude': _points.longitude,
      'country': _points.country
    };

    Map data = {
      'business_name': businessName.trim(),
      'business_number': businessNumber.trim(),
      'identity_number': identityNumber.trim(),
      'business_address': address,
      'business_address_detail': pdto.trim(),
      'terms': _blockCheck,
    };

    final value = sharedPreferences.get('token');

    var response = await http.post(API + 'provider/',
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ' + value
        },
        body: jsonEncode(data));

    if (response.statusCode == 200 || response.statusCode == 201) {
      return response.statusCode;
    } else if (response.statusCode == 400) {
      return response.statusCode;
    } else {
      throw Exception('Failed to load post');
    }
  }

  registerServices(
      String title,
      description,
      dateActivityInit,
      dateActivityFinish,
      timeActivityInit,
      timeActivityFinish,
      _points,
      price,
      observation) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    Map address = {
      'address': _points.address,
      'country': _points.country,
      'latitude': _points.latitude,
      'longitude': _points.longitude,
    };

    Map positions = {
      'latitude': _points.latitude,
      'longitude': _points.longitude,
    };
    Map data = {
      'title': title.trim(),
      'description': description.trim(),
      'start_date': dateActivityInit.trim(),
      'end_date': dateActivityFinish.trim(),
      'start_time': timeActivityInit.trim(),
      'end_time': timeActivityFinish.trim(),
      'address': address,
      'location_coords': positions,
      'price': price.trim(),
      'observations': observation.trim(),
    };

    final value = sharedPreferences.get('token');

    var response = await http.post(API + 'service/',
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer ' + value
        },
        body: jsonEncode(data));
    print(response.body);
    print(response.statusCode);
    if (response.statusCode == 200 || response.statusCode == 201) {
      return response.statusCode;
    } else if (response.statusCode == 400) {
      return response.statusCode;
    } else {
      throw Exception('Failed to load post');
    }
  }
}
