import 'package:fitgage/src/pages/home.dart';
import 'package:flutter/material.dart';

import 'package:fitgage/routes/routes.dart';
import 'package:fitgage/src/pages/login.dart';
import 'package:get_it/get_it.dart';
import 'package:fitgage/services/services.dart';
import 'package:fitgage/widgets/validations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:fitgage/widgets/inputs.dart';
import 'package:provider/provider.dart';
import 'package:fitgage/providers/data_provider.dart';


//Package para reutlizar clases
void setupLocator() {
  GetIt.I.registerLazySingleton(() => AppService());
  GetIt.I.registerLazySingleton(() => Validations());
  GetIt.I.registerLazySingleton(() => Inputs());
}

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => DataInfo(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Fitgage',
        theme: ThemeData(fontFamily: 'Gentona'),
        localizationsDelegates: [
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('es', 'ES'),
        ],
        routes: getApplicationRoutes(),
        onGenerateRoute: (RouteSettings settings) {
          return MaterialPageRoute(builder: (BuildContext context) => Login());
        },
      ),
    );
  }
}
