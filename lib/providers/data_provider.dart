import 'package:flutter/material.dart';

class DataInfo with ChangeNotifier {
  bool _checkOkData = true;

  get checkOkData {
    return _checkOkData;
  }

  set checkOkData(bool checkOkData) {
    this._checkOkData = false;
    notifyListeners();
  }

  bool _checkOKMP = true;

  get checkOKMP {
    return _checkOKMP;
  }

  set checkOKMP(bool checkOKMP) {
    this._checkOKMP = false;
    notifyListeners();
  }
}
