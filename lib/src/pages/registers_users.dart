import 'package:flutter/material.dart';

import 'package:fitgage/services/services.dart';
import 'package:get_it/get_it.dart';
import 'package:fitgage/src/pages/register_data.dart';
import 'package:fitgage/widgets/inputs.dart';

class RegisterUsers extends StatefulWidget {
  @override
  _RegisterUsers createState() => _RegisterUsers();
}

class _RegisterUsers extends State<RegisterUsers> {
  bool _isLoading = false;
  final _formKey = new GlobalKey<FormState>();

  AppService get services => GetIt.I<AppService>();
  Inputs get inputs => GetIt.I<Inputs>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xFFf1f2f6),
        child: new Container(
            margin: EdgeInsets.all(30.0),
            child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : new Form(
                    key: _formKey,
                    child: formUI(),
                  )),
      ),
    );
  }

  Widget formUI() {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text(
              'Registro',
              style: TextStyle(
                  color: Color(0xFF35383d),
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1),
            ),
            inputs.logo(),
            SizedBox(height: 60.0),
            inputs.inputEmail(),
            Container(height: 30.0),
            inputs.inputPassword(),
            SizedBox(height: 40.0),
            _buildRegister(),
            SizedBox(height: 25.0),
            _buildLoginBtn()
          ],
        ),
      ),
    );
  }

  Widget _buildRegister() {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        elevation: 0.0,
        padding: EdgeInsets.all(17.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        color: Colors.orange[500],
        child: Text(
          'REGISTRARSE',
          style: TextStyle(
              color: Color(0xFFf1f2f6),
              letterSpacing: 1,
              fontWeight: FontWeight.bold,
              fontSize: 15),
        ),
        onPressed: () {
          if (_formKey.currentState.validate() == true) {
            setState(() async {
              _isLoading = true;
              var res = await services.registry(
                  usernameController.text, passwordController.text);
              if (res == 400) {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          content: Text('The user already exists.'),
                          actions: <Widget>[
                            FlatButton(
                                child: Text('OK'),
                                onPressed: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => RegisterUsers()),
                                  );
                                })
                          ],
                        ));
              } else if (res == 200 || res == 201) {
                Navigator.of(context).pushAndRemoveUntil(
                    MaterialPageRoute(
                        builder: (BuildContext context) => RegisterData()),
                    (Route<dynamic> route) => false);
              }
            });
          }
        },
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
        alignment: Alignment.centerRight,
        child: Center(
          child: FlatButton(
              onPressed: () => {Navigator.pushNamed(context, '/login')},
              child: Text(
                '¿No tienes una cuenta? Iniciar sesión',
                style: TextStyle(color: Color(0xFF323338), fontSize: 13),
              )),
        ));
  }
}
