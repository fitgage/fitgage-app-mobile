import 'package:flutter/material.dart';

import 'package:image_picker/image_picker.dart';
import 'dart:io';


class RegisterFitMedical extends StatefulWidget {
  @override
  _RegisterFitMedical createState() => _RegisterFitMedical();
}

class _RegisterFitMedical extends State<RegisterFitMedical> {
  File _image;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Apto medico'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _setImageView(),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _showSelectionDialog(context);
        },
        child: Icon(Icons.camera_alt),
      ),
    );
  }

  Future<void> _showSelectionDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text("Seleccione de donde desea sacar la foto"),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    GestureDetector(
                      child: Text("Galeria"),
                      onTap: () {
                        _openGallery(context);
                      },
                    ),
                    Padding(padding: EdgeInsets.all(8.0)),
                    GestureDetector(
                      child: Text("Camara"),
                      onTap: () {
                        _openCamera(context);
                      },
                    )
                  ],
                ),
              ));
        });
  }

  void _openGallery(BuildContext context) async {
    final picker = ImagePicker();
    var picture = await picker.getImage(source: ImageSource.gallery);

    this.setState(() {
      _image = File(picture.path);
    });
    Navigator.of(context).pop();
  }

  void _openCamera(BuildContext context) async {
    final picker = ImagePicker();
    var picture = await picker.getImage(source: ImageSource.camera);
    this.setState(() {
      _image = File(picture.path);
    });
    Navigator.of(context).pop();
  }

  Widget _setImageView() {
    if (_image != null) {
      return Image.file(_image, width: 500, height: 500);
    } else {
      return Text("Por favor seleccione una imagen");
    }
  }
}
