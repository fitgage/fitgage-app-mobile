import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/home.dart';

class MyProfile extends StatefulWidget {
  @override
  _MyProfile createState() => _MyProfile();
}

class _MyProfile extends State<MyProfile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MainPage()),
                );
              })
        ],
        backgroundColor: Colors.blue,
        title: Text("Mi Perfil"),
      ),
    );
  }
}
