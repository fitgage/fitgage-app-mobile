import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/login.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:fitgage/src/pages/my_profile.dart';
import 'package:fitgage/src/pages/teacher/register_teacher.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fitgage/src/pages/teacher/actions_activities/buy_activity.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/contracted_activities.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/favorite_activities.dart';
import 'package:fitgage/src/pages/register_fitmedical_users.dart';
import 'package:fitgage/widgets/inputs.dart';
import 'package:fitgage/src/pages/teacher/profile_teacher.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  SharedPreferences sharedPreferences;
  String value = '';

  Inputs get inputs => GetIt.I<Inputs>();

  List activitiesData;

  @override
  void initState() {
    super.initState();
    checkLoginStatus();
  }

  Future checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if (sharedPreferences.getString("token") == null) {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (BuildContext context) => Login()),
          (Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: Container(
          color: Colors.white,
          child: new ListView(
            children: <Widget>[
              Container(
                height: 30,
                decoration: BoxDecoration(),
              ),
              Container(
                alignment: Alignment(-1.5, 0.0),
                width: 50.0,
                height: 100.0,
                child: CircleAvatar(
                  radius: 100.0,
                  backgroundColor: Colors.transparent,
                  child: ClipOval(
                    child: Image.asset(
                      'assets/user.jpeg',
                      width: 100,
                      height: 100,
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              new ListTile(
                title: new Text(
                  "Robert Smith",
                  style: TextStyle(
                      color: Color(0xFF4a4a4a),
                      fontSize: 30,
                      fontWeight: FontWeight.bold),
                ),
                subtitle: new Text(
                  "0 ACTIVIDADES",
                  style: TextStyle(
                    color: Color(0xFFa6a9b2),
                  ),
                ),
              ),
              new ListTile(),
              new ListTile(
                title: new Text(
                  "Mi Perfil",
                  style: TextStyle(
                    color: Color(0xFF656565),
                    fontSize: 14,
                  ),
                ), //656565
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                      color: Color(0xFFf4f4f5), shape: BoxShape.circle),
                  child: Icon(
                    Icons.person,
                    color: Color(0xFFa4a7b0),
                    size: 18.0,
                  ),
                ),
                dense: true,
                onTap: () => Navigator.of(context).push(new MaterialPageRoute(
                  builder: (BuildContext context) => MyProfile(),
                )),
              ),
              new ListTile(
                title: new Text(
                  "Convertirse en Profesor",
                  style: TextStyle(
                    color: Color(0xFF656565),
                    fontSize: 14,
                  ),
                ),
                dense: true,
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                      color: Color(0xFFf4f4f5), shape: BoxShape.circle),
                  child: Icon(
                    Icons.supervisor_account,
                    color: Color(0xFFa4a7b0),
                    size: 18.0,
                  ),
                ),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RegisterTeacher()),
                ),
              ),
              new ListTile(
                title: new Text(
                  "Actividades Contratadas",
                  style: TextStyle(
                    color: Color(0xFF656565),
                    fontSize: 14,
                  ),
                ),
                dense: true,
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                      color: Color(0xFFf4f4f5), shape: BoxShape.circle),
                  child: Icon(
                    FontAwesomeIcons.running,
                    color: Color(0xFFa4a7b0),
                    size: 18.0,
                  ),
                ),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ContractedActity()),
                ),
              ),
              new ListTile(
                title: new Text(
                  "Actividades Favoritas",
                  style: TextStyle(
                    color: Color(0xFF656565),
                    fontSize: 14,
                  ),
                ),
                dense: true,
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                      color: Color(0xFFf4f4f5), shape: BoxShape.circle),
                  child: Icon(
                    Icons.favorite,
                    color: Color(0xFFa4a7b0),
                    size: 18.0,
                  ),
                ),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => FavoriteActivities()),
                ),
              ),
              new ListTile(
                title: new Text(
                  "Sube tu Apto Medico",
                  style: TextStyle(
                    color: Color(0xFF656565),
                    fontSize: 14,
                  ),
                ),
                dense: true,
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                      color: Color(0xFFf4f4f5), shape: BoxShape.circle),
                  child: Icon(
                    Icons.assignment,
                    color: Color(0xFFa4a7b0),
                    size: 18.0,
                  ),
                ),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => RegisterFitMedical()),
                ),
              ),
              new ListTile(
                title: new Text(
                  "Perfil del profesor",
                  style: TextStyle(
                    color: Color(0xFF656565),
                    fontSize: 14,
                  ),
                ),
                dense: true,
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                      color: Color(0xFFf4f4f5), shape: BoxShape.circle),
                  child: Icon(
                    Icons.assignment,
                    color: Color(0xFFa4a7b0),
                    size: 18.0,
                  ),
                ),
                onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ProfileTeacher()),
                ),
              ),
              new ListTile(),
              new ListTile(),
              new ListTile(
                title: new Text(
                  "Log Out",
                  style: TextStyle(
                    color: Color(0xFF656565),
                    fontSize: 14,
                  ),
                ),
                //leading: new Icon(Icons.exit_to_app),
                dense: true,
                leading: Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                      color: Color(0xFFf4f4f5), shape: BoxShape.circle),
                  child: Icon(
                    Icons.exit_to_app,
                    color: Color(0xFFa4a7b0),
                    size: 18.0,
                  ),
                ),
                onTap: () => {
                  sharedPreferences.clear(),
                  SystemNavigator.pop(),
                },
              ),
            ],
          ),
        ),
      ),
      appBar: AppBar(
        backgroundColor: Color(0xFFf1f2f6),
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'FITGAGE',
          style: TextStyle(
              color: Color(0xFFff4700),
              letterSpacing: 1,
              fontFamily: "RB Black Italic",
              //fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        iconTheme: IconThemeData(color: Colors.black45),
      ),
      body: Container(
        color: Color(0xFFf1f2f6),
        child: Container(
          margin: EdgeInsets.all(25.0),
          child: Center(
            child: Column(
              children: <Widget>[
                inputs.inputAddressSearch(context),
                Container(height: 15.0),
                _btnSearchActivities(),
                Container(height: 15.0),
                _listActivities(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _btnSearchActivities() {
    return Container(
        width: double.infinity,
        child: RaisedButton(
            elevation: 0.0,
            padding: EdgeInsets.all(18.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(8.0)),
            color: Colors.orange[500],
            child: Text('Buscar Actividades',
                style: TextStyle(
                    color: Color(0xFFf1f2f6),
                    letterSpacing: 1,
                    fontWeight: FontWeight.bold,
                    fontSize: 15)),
            onPressed: () {
              getActivities();
            }));
  }

  Widget _listActivities() {
    return Expanded(
        child: ListView.builder(
      itemCount: activitiesData == null ? 0 : activitiesData.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          margin: EdgeInsets.all(5.0),
          elevation: 10.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(1.0)),
          child: Column(
            children: <Widget>[
              Image(
                  image: NetworkImage(
                      'https://awakeningclaritynow.com/wp-content/uploads/Competitive-men-running-350x150.jpg'),
                  fit: BoxFit.contain),
              ListTile(
                title: Text("${activitiesData[index]["title"]}"),
                subtitle: Text("${activitiesData[index]["description"]}"),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton.icon(
                      padding: EdgeInsets.all(0.8),
                      icon: Icon(
                        Icons.star,
                        color: Colors.yellow,
                      ),
                      label: Text('4.8'),
                      onPressed: () {}),
                  FlatButton.icon(
                      padding: EdgeInsets.all(0.8),
                      icon: Icon(
                        FontAwesomeIcons.heart,
                        color: Colors.red,
                      ),
                      label: Text(''),
                      onPressed: () {}),
                  FlatButton.icon(
                      padding: EdgeInsets.all(0.8),
                      icon: Icon(FontAwesomeIcons.wallet, color: Colors.black),
                      label: Text("${activitiesData[index]["price"]}"),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                                  content: Text(
                                      'Excelente elección, vas a contratar: \n' +
                                          '\n'
                                              '${activitiesData[index]["title"]}.\n'
                                              '\n' +
                                          'Acepta y resérvarla'),
                                  actions: <Widget>[
                                    FlatButton(
                                        child: Text('Aceptar'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      BuyActivity()));
                                        }),
                                    FlatButton(
                                        child: Text('Cancel'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      MainPage()));
                                        }),
                                  ],
                                ));
                      }),
                ],
              )
            ],
          ),
        );
      },
    ));
  }

  getActivities() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    final value = sharedPreferences.get('token');
    var response = await http.get(
      'https://dev.fitgage.us/api/v1/service',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ' + value
      },
    );

    var data = json.decode(response.body);
    setState(() {
      activitiesData = data;
    });
  }
}
