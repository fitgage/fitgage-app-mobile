import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/home.dart';
import 'package:geolocator/geolocator.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:fitgage/services/services.dart';
import 'package:fitgage/widgets/inputs.dart';

class RegisterData extends StatefulWidget {
  @override
  _RegisterData createState() => _RegisterData();
}

class _RegisterData extends State<RegisterData> {
  bool _isLoading = false;
  final _formKey = new GlobalKey<FormState>();
  String _date = '';
  String value;
  Position _currentPosition;
  String _currentAddress;
  Placemark _location;

  final Geolocator geolocator = Geolocator()..forceAndroidLocationManager;

  AppService get services => GetIt.I<AppService>();
  Inputs get inputs => GetIt.I<Inputs>();

  final TextEditingController birthdateController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFf1f2f6),
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'FITGAGE',
          style: TextStyle(
              color: Color(0xFFff4700),
              letterSpacing: 1,
              fontFamily: "RB Black Italic",
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
      ),
      body: Container(
        color: Color(0xFFf1f2f6),
        height: double.infinity,
        child: new Container(
          margin: EdgeInsets.all(20.0),
          child: SingleChildScrollView(
              child: _isLoading
                  ? Center(child: CircularProgressIndicator())
                  : new Form(
                      key: _formKey,
                      child: _formUI(),
                    )),
        ),
      ),
    );
  }

  Widget _formUI() {
    return Center(
        child: Column(
      children: <Widget>[
        Text(
          'Datos personales',
          style: TextStyle(
              fontSize: 25, letterSpacing: 1, fontWeight: FontWeight.bold),
        ),
        Container(height: 40.0),
        inputs.inputName(),
        Container(height: 25.0),
        inputs.inputSurname(),
        Container(height: 25.0),
        _inputBirthdate(),
        Container(height: 25.0),
        _inputLocation(),
        Container(height: 80.0),
        _buildRegisterData(),
      ],
    ));
  }

  Widget _inputBirthdate() {
    return TextField(
      enableInteractiveSelection: false,
      controller: birthdateController,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        icon: Icon(Icons.calendar_today),
        labelText: 'Fecha de Nacimiento',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate: new DateTime(2002),
        firstDate: new DateTime(1900),
        lastDate: new DateTime(2002),
        locale: Locale('es', 'ES'));

    if (picked != null) {
      setState(() {
        _date = picked.toString();
        birthdateController.text =
            DateFormat('MM/dd/yyyy').format(DateTime.parse(_date));
      });
    }
  }

  Widget _inputLocation() {
    return Container(
        child: Row(children: <Widget>[
      Container(
        margin: EdgeInsets.only(right: 15.0),
        child: Icon(
          Icons.location_on,
          color: Color(0xFF858688),
        ),
      ),
      Container(
           width: 280,
          child: ButtonTheme(
              alignedDropdown: true,
              child: DropdownButton(
                isExpanded: true,
                hint: (value != null && _currentAddress != null)
                    ? Text(
                        _currentAddress,
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      )
                    : Text('Ubicación'),
                iconSize: 30.0,
                style: TextStyle(color: Colors.black),
                underline: Container(
                  decoration: const BoxDecoration(
                      border: Border(bottom: BorderSide(color: Colors.grey))),
                ),
                items: ['Usar mi ubicación'].map(
                  (val) {
                    return DropdownMenuItem<String>(
                      value: val,
                      child: Row(
                        //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Icon(Icons.my_location, color: Color(0xFF858688)),
                          SizedBox(width: 10),
                          Text(val),
                        ],
                      ),
                    );
                  },
                ).toList(),
                onChanged: (val) {
                  setState(
                    () async {
                      await _getCurrentLocation();
                      value = val;
                    },
                  );
                },
              ))),
    ]));
  }

  _getCurrentLocation() async {
    geolocator
        .getCurrentPosition(desiredAccuracy: LocationAccuracy.best)
        .then((Position position) {
      setState(() {
        _currentPosition = position;
      });

      _getAddressFromLatLng();
    }).catchError((e) {
      print(e);
    });
  }

  _getAddressFromLatLng() async {
    try {
      List<Placemark> p = await geolocator.placemarkFromCoordinates(
          _currentPosition.latitude, _currentPosition.longitude);

      Placemark place = p[0];

      setState(() {
        _location = place;
        _currentAddress =
            "${place.thoroughfare},${place.subThoroughfare}, ${place.postalCode}, ${place.country}";
      });
    } catch (e) {
      print(e);
    }
  }

  Widget _buildRegisterData() {
    return Container(
      width: double.infinity,
      child: RaisedButton(
          elevation: 0.0,
          padding: EdgeInsets.all(17.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
          color: Colors.orange[500],
          child: Text('Guardar',
              style: TextStyle(
                  color: Color(0xFFf1f2f6),
                  letterSpacing: 1,
                  fontWeight: FontWeight.bold,
                  fontSize: 15)),
          onPressed: () {
            if (_formKey.currentState.validate() == true) {
              setState(() async {
                // _isLoading = true;
                var dateFormat = new DateFormat('yyyy-MM-dd');
                String formattedDate = dateFormat.format(DateTime.parse(_date));

                var res = await services.registerData(nameController.text,
                    surnameController.text, formattedDate, _location);
                if (res == 200 || res == 201) {
                  Navigator.of(context).pushAndRemoveUntil(
                      MaterialPageRoute(
                          builder: (BuildContext context) => MainPage()),
                      (Route<dynamic> route) => false);
                } else if (res == 400) {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            content: Text('Incorrect data'),
                            actions: <Widget>[
                              FlatButton(
                                  child: Text('OK'),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => RegisterData()),
                                    );
                                  })
                            ],
                          ));
                }
              });
            }
          }),
    );
  }
}
