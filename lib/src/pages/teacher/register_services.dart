import 'package:flutter/material.dart';

import 'package:date_format/date_format.dart';
import 'package:fitgage/services/services.dart';
import 'package:fitgage/src/pages/teacher/services_teacher.dart';
import 'package:fitgage/widgets/inputs.dart';
import 'package:get_it/get_it.dart';
import 'package:address_search_field/address_search_field.dart';

class RegisterServices extends StatefulWidget {
  RegisterServices({Key key}) : super(key: key);

  @override
  _RegisterServices createState() => _RegisterServices();
}

class _RegisterServices extends State<RegisterServices> {
  bool _isLoading = false;
  final _formKey = new GlobalKey<FormState>();
  DateTime _dateInit = new DateTime.now();
  String dateFormattedInit;
  String dateFormattedFinish;
  DateTime _dateFinish = new DateTime.now();
  TimeOfDay _timeInit = new TimeOfDay.now();
  TimeOfDay _timeFinish = new TimeOfDay.now();
  AddressPoint _points;

  AppService get services => GetIt.I<AppService>();
  Inputs get inputs => GetIt.I<Inputs>();

  final TextEditingController dateActivityInitController =
      new TextEditingController();
  final TextEditingController dateActivityFinishController =
      new TextEditingController();
  final TextEditingController timeActivityInitController =
      new TextEditingController();
  final TextEditingController timeActivityFinishController =
      new TextEditingController();
  final TextEditingController addressSearchController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        centerTitle: true,
        title: Text(
          'Completa tu Actividad',
          style: TextStyle(color: Colors.black45),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              size: 35,
              color: Colors.black45,
            ),
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ServicesTeacher()));
            }),
      ),
      body: Container(
        margin: EdgeInsets.all(35.0),
        child: SingleChildScrollView(
          child: _isLoading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : new Form(
                  key: _formKey,
                  child: _formUI(),
                ),
        ),
      ),
    );
  }

  Widget _formUI() {
    return Container(
      child: Column(
        children: <Widget>[
          Container(height: 1.0),
          Image.asset(
            'assets/logo.png',
            width: 220,
            height: 150,
          ),
          Container(height: 5.0),
          inputs.inputTitle(),
          Container(height: 15.0),
          inputs.inputDesciption(),
          Container(height: 10.0),
          _inputDateActivityInit(),
          Container(height: 10.0),
          _inputDateActivityFinish(),
          Container(height: 10.0),
          _inputTimeActivityInit(),
          Container(height: 10.0),
          _inputTimeActivityFinish(),
          Container(height: 10.0),
          _inputAddressActivity(),
          Container(height: 10.0),
          inputs.inputPriceActivity(),
          Container(height: 10.0),
          inputs.inputObservations(),
          Container(height: 10.0),
          _buildSuccessActivity(),
          Container(height: 20.0),
        ],
      ),
    );
  }

//Init Activity
  Widget _inputDateActivityInit() {
    return TextField(
      enableInteractiveSelection: false,
      controller: dateActivityInitController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Cuando comienza tu actividad',
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDate(context);
      },
    );
  }

  _selectDate(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime.now(),
      lastDate: new DateTime(2030),
    );

    if (picked != null) {
      setState(() {
        _dateInit = picked;
        dateActivityInitController.text =
            formatDate(_dateInit, [dd, '-', mm, '-', yyyy]);
        dateFormattedInit = (formatDate(_dateInit, [yyyy, '-', mm, '-', dd]));
      });
    }
  }

//Finish Activity
  Widget _inputDateActivityFinish() {
    return TextField(
      enableInteractiveSelection: false,
      controller: dateActivityFinishController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Cuando termina tu actividad',
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectDateFinish(context);
      },
    );
  }

  _selectDateFinish(BuildContext context) async {
    DateTime picked = await showDatePicker(
      context: context,
      initialDate: new DateTime.now(),
      firstDate: new DateTime.now(),
      lastDate: new DateTime(2030),
    );

    if (picked != null) {
      setState(() {
        _dateFinish = picked;
        dateActivityFinishController.text =
            formatDate(_dateFinish, [dd, '-', mm, '-', yyyy]);
        dateFormattedFinish =
            (formatDate(_dateFinish, [yyyy, '-', mm, '-', dd]));
      });
    }
  }

//Init Hours
  Widget _inputTimeActivityInit() {
    return TextField(
      enableInteractiveSelection: false,
      controller: timeActivityInitController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Horario que comienza tu actividad',
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectHoursInit(context);
      },
    );
  }

  _selectHoursInit(BuildContext context) async {
    TimeOfDay picked = await showTimePicker(
        context: context, initialTime: new TimeOfDay.now());

    if (picked != null) {
      setState(() {
        _timeInit = picked;
        //Formatea
        final MaterialLocalizations localizations =
            MaterialLocalizations.of(context);
        final String formattedTimeOfDay =
            localizations.formatTimeOfDay(_timeInit);
        print(formattedTimeOfDay);
        timeActivityInitController.text = formattedTimeOfDay;
      });
    }
  }

//Finish Hours
  Widget _inputTimeActivityFinish() {
    return TextField(
      enableInteractiveSelection: false,
      controller: timeActivityFinishController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Horario que finaliza tu actividad',
      ),
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
        _selectHoursFinish(context);
      },
    );
  }

  _selectHoursFinish(BuildContext context) async {
    TimeOfDay picked = await showTimePicker(
        context: context, initialTime: new TimeOfDay.now());
    if (picked != null) {
      setState(() {
        _timeFinish = picked;
        final MaterialLocalizations localizations =
            MaterialLocalizations.of(context);
        final String formattedTimeOfDay =
            localizations.formatTimeOfDay(_timeFinish);
        timeActivityFinishController.text = formattedTimeOfDay;
        print(formattedTimeOfDay);
      });
    }
  }

// NO tiene validación
  Widget _inputAddressActivity() {
    return AddressSearchField(
      country: "Argentina",
      noResultsText: 'No existen resultados',
      hintText: 'Busca la dirección',
      controller: addressSearchController,
      decoration: InputDecoration(
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Dirección de la Actividad',
      ),
      onDone: (AddressPoint point) {
        _points = point;
        print('points');
        print(_points);
        Navigator.of(context).pop();
      },
    );
  }

  Widget _buildSuccessActivity() {
    return Container(
        width: double.infinity,
        child: RaisedButton(
          elevation: 5.0,
          padding: EdgeInsets.all(18.0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
              side: BorderSide(color: Colors.black)),
          color: Colors.orange[600],
          child: Text(
            'Publicar',
            style: TextStyle(
                letterSpacing: 1, fontWeight: FontWeight.bold, fontSize: 18),
          ),
          onPressed: () {
            if (_formKey.currentState.validate() == true) {
              setState(() async {
                var res = await services.registerServices(
                    titleController.text,
                    descriptionController.text,
                    dateFormattedInit,
                    dateFormattedFinish,
                    timeActivityInitController.text,
                    timeActivityFinishController.text,
                    _points,
                    priceController.text,
                    observationsController.text);
                if (res == 200 || res == 201) {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            title: Text('Muchas Gracias!'),
                            content: Text('Actividad creada correctamente!.'),
                            actions: <Widget>[
                              FlatButton(
                                  child: Text('OK'),
                                  onPressed: () {
                                    Navigator.of(context).pushAndRemoveUntil(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                ServicesTeacher()),
                                        (Route<dynamic> route) => false);
                                  })
                            ],
                          ));
                }
                if (res == 400) {
                  showDialog(
                      context: context,
                      builder: (context) => AlertDialog(
                            content: Text('Incorrect data'),
                            actions: <Widget>[
                              FlatButton(
                                  child: Text('OK'),
                                  onPressed: () {
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              RegisterServices()),
                                    );
                                  })
                            ],
                          ));
                }
              });
            }
          },
        ));
  }
}
