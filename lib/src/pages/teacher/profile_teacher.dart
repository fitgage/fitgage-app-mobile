import 'package:flutter/material.dart';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:fitgage/src/pages/home.dart';

class ProfileTeacher extends StatefulWidget {
  const ProfileTeacher({Key key}) : super(key: key);

  @override
  _ProfileTeacherState createState() => _ProfileTeacherState();
}

class _ProfileTeacherState extends State<ProfileTeacher> {
  SharedPreferences sharedPreferences;
  String name;
  String lastname;


  @override
  void initState() {
    super.initState();
    checkdata();
  }

  checkdata() async {
    sharedPreferences = await SharedPreferences.getInstance();
    setState(() {
      if (sharedPreferences.getString("name") != null) {
        name = sharedPreferences.getString("name");
      } else if (sharedPreferences.getString("lastname") != null) {
            lastname = sharedPreferences.getString("lastname");

      } else {
        name = 'No hay datos';
        lastname = 'No hay datos';
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFf1f2f6),
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'FITGAGE',
          style: TextStyle(
              color: Color(0xFFff4700),
              letterSpacing: 1,
              fontFamily: "RB Black Italic",
              fontWeight: FontWeight.bold,
              fontSize: 18),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey,
            ),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => MainPage()));
            }),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Container(
      color: Colors.white,
      height: double.infinity,
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _image(),
            _card(),
          ],
        ),
      ),
    );
  }

  Widget _image() {
    return Container(
      height: 450,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/user.jpeg'), fit: BoxFit.cover)),

    );
  }


  Widget _card() {
    return Container(
      height: 175,
      child: Card(
        elevation: 0.0,
        child: Column(
          children: <Widget>[
            ListTile(
              title: Text('$name + $lastname'),
              subtitle: Text(
                  'Mas de 15 años en el sector evaluando las capacidades físicas y las necesidades de sus alumnos, y organizan sus clases a medida para satisfacer esas necesidades. Algunas clases se centran más en la respiración y la meditación, mientras que otras pueden resultar muy exigentes físicamente.'),
            ),
          ],
        ),
      ),
    );
  }
}