import 'package:fitgage/providers/data_provider.dart';
import 'package:fitgage/src/pages/teacher/register_teacher.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:webview_flutter/webview_flutter.dart';
import 'dart:async';

class LoginMercadoPago extends StatefulWidget {
  @override
  _LoginMercadoPago createState() => _LoginMercadoPago();
}

class _LoginMercadoPago extends State<LoginMercadoPago> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  @override
  Widget build(BuildContext context) {
    final dataInfo = Provider.of<DataInfo>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Vincula tu cuenta'),
        actions: <Widget>[],
      ),
      body: Builder(builder: (BuildContext context) {
        return WebView(
          initialUrl:
              'https://auth.mercadopago.com.ar/authorization?client_id=26847808526915&response_type=code&platform_id=mp&redirect_uri=https://dev.fitgage.us/mp-marketplace-credentials/link-accounts/user/10',
          onPageStarted: (String url) {
            dataInfo.checkOKMP = false;
            if (url ==
                'https://dev.fitgage.us/mp-marketplace-credentials/link-accounts/finish') {
              Navigator.of(context).push(new MaterialPageRoute(
                  builder: (context) => RegisterTeacher()));
            }
          },
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
        );
      }),
    );
  }
}
