import 'package:fitgage/src/pages/teacher/services_teacher.dart';
import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/home.dart';
import 'package:fitgage/src/pages/teacher/register_data_teacher.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:fitgage/src/pages/teacher/login_mercado_pago.dart';
import 'package:fitgage/providers/data_provider.dart';

const API = 'http://vps-859aa2c8.vps.ovh.net:8000/api/v1/';

class RegisterTeacher extends StatefulWidget {
  @override
  _RegisterTeacher createState() => _RegisterTeacher();
}

class _RegisterTeacher extends State<RegisterTeacher> {
  final textStyleTitle = new TextStyle(fontSize: 20, color: Colors.black54);
  final textStylesubtitle = new TextStyle(fontSize: 10, color: Colors.black26);

  Widget build(BuildContext context) {
    final DataInfo dataInfo = Provider.of<DataInfo>(context);

    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar(
          elevation: 0.0,
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text(
            'Perfil del Profesor',
            style: TextStyle(color: Colors.black45),
          ),
          leading: IconButton(
              icon: Icon(
                Icons.arrow_back,
                size: 25,
                color: Colors.black45,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MainPage()),
                );
              }),
        ),
      ),
      body: Container(
        margin: EdgeInsets.all(0.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              children: <Widget>[
                Container(
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                      image: new AssetImage("assets/trainer_profile.jpg"),
                      fit: BoxFit.fitWidth,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                  height: 330,
                  margin: new EdgeInsets.only(top: 00.0),
                ),
                Container(
                    padding: const EdgeInsets.fromLTRB (16.0, 16.0, 16.0, 0.0),
                    width: MediaQuery.of(context).size.width*0.98,
                    child: new Column (
                        children: <Widget>[ Text(
                          'Completa los siguientes pasos para aplicar como Profesor en Fitgage.',
                          style:  TextStyle(fontSize: 20, color: Color(0Xff0c0c0c))
                        )]
                    )
                ),
                Container(
                  padding: const EdgeInsets.fromLTRB (16.0, 10.0, 16.0, 0.0),
                  width: MediaQuery.of(context).size.width*0.98,
                  child: new Column (
                    children: <Widget>[ Text(
                      'Una vez finalizado podrás generar tus actividades.',
                      style:  TextStyle(fontSize: 14, color: Colors.black26),
                    )]
                  )
                ),
                Container(
                    padding: const EdgeInsets.only (top: 16.0),
                    child: SingleChildScrollView(
                      child: new Card(
                          elevation: 0.0,
                          color: Colors.transparent,
                          child: ListView(
                          shrinkWrap: true,
                          children: <Widget>[
                            new ListTile(
                                enabled: dataInfo.checkOkData,
                                contentPadding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                title: new Text('Datos Comerciales',
                                    style:  TextStyle(fontSize: 17, color: Color(0Xff636363))
                                ),
                                subtitle: new Text(
                                    'Comencemos con tus datos comerciales',
                                    style:  TextStyle(fontSize: 14, color: Color(0xFF7e8287))),
                                dense: true,
                                leading: Container(
                                  width: 40.0,
                                  height: 40.0,
                                  decoration: new BoxDecoration(
                                      color: Color(0xFFf4f4f5),
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(
                                    Icons.assignment_ind,
                                    color: Color(0xFFa4a7b0),
                                    size: 18.0,
                                  ),
                                ),
                                trailing: (dataInfo.checkOkData != null && dataInfo.checkOkData
                                    ? Icon(Icons.arrow_forward_ios, size: 15.0)
                                    : Icon(Icons.check_box,
                                        size: 15.0,
                                        color: Colors.blue,
                                      )),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            RegisterDataTeacher()),
                                  );
                                }),
                            new ListTile(
                                enabled: dataInfo.checkOKMP,
                                contentPadding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                                title: new Text('Mercado Pago',
                                    style:  TextStyle(fontSize: 17, color: Color(0Xff636363))
                                ),
                                subtitle: new Text(
                                    'Vincula tu cuenta, asi podés cobrar por tus servicios',
                                    style:  TextStyle(fontSize: 14, color: Color(0xFF7e8287))),
                                dense: true,
                                leading: Container(
                                  width: 40.0,
                                  height: 40.0,
                                  decoration: new BoxDecoration(
                                      color: Color(0xFFf4f4f5),
                                      shape: BoxShape.circle
                                  ),
                                  child: Icon(
                                    Icons.repeat ,
                                    color: Color(0xFFa4a7b0),
                                    size: 18.0,
                                  ),
                                ),
                                trailing: (dataInfo.checkOKMP != null &&
                                        dataInfo.checkOKMP
                                    ? Icon(Icons.arrow_forward_ios, size: 15.0,)
                                    : Icon(
                                        Icons.check_box,
                                        color: Colors.blue,
                                      )),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginMercadoPago()),
                                  );
                                }),
                            new ListTile(
                              contentPadding: EdgeInsets.fromLTRB(15.0, 0.0, 15.0, 0.0),
                              title: new Text('Nueva actividad',
                                  style:  TextStyle(fontSize: 17, color: Color(0Xff636363))
                              ),
                              subtitle: new Text('Empieza a Enseñar!',
                                  style:  TextStyle(fontSize: 14, color: Color(0xFF7e8287))
                              ),
                              dense: true,
                              leading: Container(
                                width: 40.0,
                                height: 40.0,
                                decoration: new BoxDecoration(
                                    color: Color(0xFFf4f4f5),
                                    shape: BoxShape.circle
                                ),
                                child: Icon(
                                  Icons.add,
                                  color: Color(0xFFa4a7b0),
                                  size: 18.0,
                                ),
                              ),
                              trailing: new Icon(Icons.arrow_forward_ios, size: 15.0,),
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ServicesTeacher(),
                                    ));
                              },
                            ),
                        ],
                      )),
                )),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
