import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:date_format/date_format.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/contracted_activities.dart';

class FinishPayActivity extends StatefulWidget {
  @override
  _FinishPayActivityState createState() => _FinishPayActivityState();
}

class _FinishPayActivityState extends State<FinishPayActivity> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFF00B575),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _payOk(),
            _payDate(),
            Divider(
              height: 50,
              color: Colors.grey,
            ),
            _summary(),
            Divider(
              height: 1,
            ),
            _footer(),
          ],
        ),
      ),
    );
  }

  Widget _payOk() {
    return Container(
      color: Color(0xFF00B575),
      height: 250,
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                alignment: Alignment.center,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF4F4F4)),
                margin: EdgeInsets.fromLTRB(0, 0, 0, 30),
                padding: EdgeInsets.all(30.0),
                child: Icon(
                  FontAwesomeIcons.shoppingBag,
                  size: 50,
                  color: Color(0xFF00B575),
                )),
            Text(
              '¡Listo! Se acreditó tu pago',
              style: TextStyle(fontSize: 23, color: Color(0xFFF4F4F4)),
            )
          ],
        ),
      ),
    );
  }

  Widget _payDate() {
    DateTime now = DateTime.now();
    final dateFormat = formatDate(now, [dd, '-', mm, '-', yyyy]);
    return Container(
      margin: EdgeInsets.fromLTRB(0, 15, 0, 0),
      child: Center(
        child: Column(
          children: <Widget>[
            Text('$dateFormat'),
            Container(height: 10),
            Text('Número de operación 125'),
          ],
        ),
      ),
    );
  }

  Widget _summary() {
    return Container(
      height: 270,
      child: Column(
        children: <Widget>[
          Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, color: Color(0xFFF3F2F0)),
              margin: EdgeInsets.fromLTRB(0, 0, 0, 10),
              padding: EdgeInsets.all(15.0),
              child: Image.asset('assets/visalogo.jpg',
                  width: (100 * 0.6), height: (100 * 0.6))),
          Column(
            children: <Widget>[
              Text(
                '\$ 522',
                style: TextStyle(fontSize: 25),
              ),
              Container(height: 10),
              Text(
                'VISA terminada en 1810',
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _footer() {
    return Container(
      child: Center(
        child: SizedBox(
          width: double.infinity,
          child: FlatButton(
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => ContractedActity()));
            },
            child: Text(
              'Continuar',
              style: TextStyle(fontSize: 23, color: Colors.blue),
            ),
          ),
        ),
      ),
    );
  }
}
