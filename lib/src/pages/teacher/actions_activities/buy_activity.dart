import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/home.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/card_pay_activity.dart';

class BuyActivity extends StatefulWidget {
  @override
  _BuyActivity createState() => _BuyActivity();
}

class _BuyActivity extends State<BuyActivity> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => MainPage()));
            }),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _title(),
            _center2Icons(),
            __center1Icons(),
            Divider(
              height: 1,
            ),
            _footer(),
          ],
        ),
      ),
    );
  }

  Widget _title() {
    return Container(
      color: Colors.blue,
      height: 90,
      child: Center(
        child: Column(
          children: <Widget>[
            Text('¿Cómo quieres pagar?',
                style: TextStyle(
                  fontSize: 23,
                  color: Color(0xFFF4F4F4),
                ))
          ],
        ),
      ),
    );
  }

  Widget _center2Icons() {
    return Container(
      height: 250,
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Color(0xFFF4EFF5)),
                  margin: EdgeInsets.fromLTRB(30, 20, 0, 10),
                  padding: EdgeInsets.all(30.0),
                  child: FlatButton.icon(
                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10000.10)),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CardPayActivity()));
                    },
                    icon: Icon(
                      FontAwesomeIcons.creditCard,
                      size: 50,
                      color: Colors.blue,
                    ),
                    label: Text(''),
                  )),
              Container(
                margin: EdgeInsets.fromLTRB(35, 10, 0, 0),
                child: Text('Nueva Tarjeta'),
              )
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Color(0xFFF4EFF5)),
                margin: EdgeInsets.fromLTRB(30, 20, 0, 10),
                padding: EdgeInsets.all(30.0),
                child: Container(
                  alignment: Alignment.center,
                  child: FlatButton.icon(
                      padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                      onPressed: () {},
                      icon: Icon(
                        Icons.attach_money,
                        size: 50,
                        color: Colors.blue,
                      ),
                      label: Text('')),
                ),
              ),
              Container(
                margin: EdgeInsets.fromLTRB(35, 10, 0, 0),
                child: Text('Pago en efectivo'),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget __center1Icons() {
    return Container(
      height: 270,
      child: Row(
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                  decoration: BoxDecoration(
                      shape: BoxShape.circle, color: Color(0xFFF4EFF5)),
                  margin: EdgeInsets.fromLTRB(30, 20, 0, 10),
                  padding: EdgeInsets.all(30.0),
                  child: FlatButton.icon(
                    padding: EdgeInsets.fromLTRB(5, 0, 0, 0),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10000.10)),
                    onPressed: () {},
                    icon: Icon(
                      FontAwesomeIcons.university,
                      size: 50,
                      color: Colors.blue,
                    ),
                    label: Text(''),
                  )),
              Container(
                margin: EdgeInsets.fromLTRB(35, 10, 0, 0),
                child: Text('Transferencia'),
              )
            ],
          )
        ],
      ),
    );
  }

  Widget _footer() {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.all(10.0),
              padding: EdgeInsets.fromLTRB(0, 10, 15, 0),
              child: Text(
                'Costo a pagar',
                style: TextStyle(fontSize: 18, color: Colors.grey),
                textAlign: TextAlign.justify,
              ),
            )
          ],
        ),
        Column(
          children: <Widget>[
            Container(
              padding: EdgeInsets.fromLTRB(0, 10, 15, 0),
              child: Text(
                '\$ 522',
                style: TextStyle(fontSize: 25, color: Colors.grey),
              ),
            )
          ],
        )
      ],
    ));
  }
}
