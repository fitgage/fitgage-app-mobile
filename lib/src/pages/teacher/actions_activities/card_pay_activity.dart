import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/teacher/actions_activities/buy_activity.dart';
import 'package:get_it/get_it.dart';
import 'package:fitgage/widgets/validations.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/finish_pay_activity.dart';

class CardPayActivity extends StatefulWidget {
  @override
  _CardPayActivityState createState() => _CardPayActivityState();
}

class _CardPayActivityState extends State<CardPayActivity> {
  Validations get check => GetIt.I<Validations>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Padding(
          padding: const EdgeInsets.only(left: 170.0),
          child: Text('Ver promociones'),
        ),
        elevation: 0.0,
        leading: BackButton(
            color: Colors.white,
            onPressed: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => BuyActivity()));
            }),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return SafeArea(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            _creditCard(),
            Container(
              height: 395,
              child: Column(
                children: <Widget>[
                  _inputNumber(),
                  _inputName(),
                  _inputSurname(),
                ],
              ),
            ),
            _footer(),
          ],
        ),
      ),
    );
  }

  Widget _creditCard() {
    return Container(
      color: Colors.blue,
      height: 250,
      child: Center(
        child: Container(
          child: Image.asset('assets/visa.png',
              width: (500 * 0.6), height: (260 * 0.6)),
        ),
      ),
    );
  }

  Widget _inputNumber() {
    return Container(
      margin: EdgeInsets.all(15.0),
      child: TextFormField(
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          contentPadding: EdgeInsets.only(left: 8),
          labelText: 'Numero de tarjeta',
        ),
        // validator: check.validateRequired,
        //controller: cuitController,
      ),
    );
  }

  Widget _inputName() {
    return Container(
      margin: EdgeInsets.all(15.0),
      child: TextFormField(
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          contentPadding: EdgeInsets.only(left: 8),
          labelText: 'Nombre',
        ),
        validator: check.validateName,
        //controller: nameController,
      ),
    );
  }

  Widget _inputSurname() {
    return Container(
      margin: EdgeInsets.all(15.0),
      child: TextFormField(
        decoration: InputDecoration(
          border: UnderlineInputBorder(),
          contentPadding: EdgeInsets.only(left: 8),
          labelText: 'Apellido',
        ),
        validator: check.validateSurname,
        //controller: surnameController,
      ),
    );
  }

  Widget _footer() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(
            children: <Widget>[
              Container(
                decoration: new BoxDecoration(
                    border: new Border.all(color: Colors.grey[400])),
                width: 205.5,
                height: 50,
                child: RaisedButton(
                  elevation: 0.0,
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => BuyActivity()));
                  },
                  child: Text('Anterior'),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              Container(
                decoration: new BoxDecoration(
                    border: new Border.all(color: Colors.grey[400])),
                width: 205.5,
                height: 50,
                child: RaisedButton(
                  elevation: 0.0,
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => FinishPayActivity()));
                  },
                  child: Text(
                    'Continuar',
                    style: TextStyle(color: Colors.blue),
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
