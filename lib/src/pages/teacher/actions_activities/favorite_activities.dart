import 'package:flutter/material.dart';


import 'package:fitgage/src/pages/home.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class FavoriteActivities extends StatefulWidget {

  @override
  _FavoriteActivities createState() => _FavoriteActivities();
}

class _FavoriteActivities extends State<FavoriteActivities> {
  List favoriteActivities;

    @override
  void initState() {
    super.initState();
    getFavoriteActivities();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFf1f2f6),
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'FITGAGE',
          style: TextStyle(
              color: Color(0xFFff4700),
              letterSpacing: 1,
              fontFamily: "RB Black Italic",
              fontSize: 18),
        ),
        leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.grey,
            ),
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => MainPage()));
            }),
        iconTheme: IconThemeData(color: Colors.black45),
      ),
      body: _body(),
    );
  }


   Widget _body() {
    return Container(
        color: Color(0xFFf1f2f6),
        child: Container(
            margin: EdgeInsets.all(25.0),
            child: Center(
                child: Column(
              children: <Widget>[
                Text('Actitivades Favoritas'),
                _listFavoriteActivities(),
              ],
            ))));
  }


  Widget _listFavoriteActivities(){
    return Expanded(
      
        child: ListView.builder(
      itemCount: favoriteActivities == null ? 0 : favoriteActivities.length - 10,
      itemBuilder: (BuildContext context, index) {
        return Card(
          margin: EdgeInsets.all(5.0),
          elevation: 10.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(1.0)),
          child: Column(
            children: <Widget>[
              Image(
                  image: NetworkImage(
                      'https://awakeningclaritynow.com/wp-content/uploads/Competitive-men-running-350x150.jpg'),
                  fit: BoxFit.contain),
              ListTile(
                title: Text("${favoriteActivities[index]["title"]}"),
                subtitle: Text("${favoriteActivities[index]["description"]}"),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton.icon(
                      padding: EdgeInsets.all(0.8),
                      icon: Icon(
                        Icons.star,
                        color: Colors.yellow,
                      ),
                      label: Text('Calificar'),
                      onPressed: () {}),
                  FlatButton.icon(
                    padding: EdgeInsets.all(0.8),
                    icon: Icon(FontAwesomeIcons.heartBroken,
                      color: Colors.red),
                      label: Text('Quitar'),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                                  content: Text(
                                      'Eliminaste esta actividad de tus favoritos'
                                              '\n' +
                                          '\n'
                                              '${favoriteActivities[index]["title"]}.\n'),
                                  actions: <Widget>[FlatButton(
                                        child: Text('OK'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      FavoriteActivities()));
                                        }),
                                  ],
                                ));
                      }),
                  FlatButton.icon(
                    padding: EdgeInsets.all(0.8),
                      icon: Icon(FontAwesomeIcons.wallet, color: Colors.black),
                      label: Text("${favoriteActivities[index]["price"]}"),
                      onPressed: null),
                ],
              )
            ],
          ),
        );
      },
    ));
  }

  getFavoriteActivities() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    final value = sharedPreferences.get('token');
    var response = await http.get(
      'https://dev.fitgage.us/api/v1/service',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ' + value
      },
    );

    var data = json.decode(response.body);
    setState(() {
      favoriteActivities = data;
    });
  }
}