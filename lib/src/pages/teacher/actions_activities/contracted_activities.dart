import 'package:flutter/material.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:fitgage/src/pages/home.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/favorite_activities.dart';
import 'package:share/share.dart';

class ContractedActity extends StatefulWidget {
  @override
  _ContractedActity createState() => _ContractedActity();
}

class _ContractedActity extends State<ContractedActity> {
  List activitiesContracted;

  @override
  void initState() {
    super.initState();
    getActivitiesContracted();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFFf1f2f6),
        elevation: 0.0,
        centerTitle: true,
        title: Text(
          'FITGAGE',
          style: TextStyle(
              color: Color(0xFFff4700),
              letterSpacing: 1,
              fontFamily: "RB Black Italic",
              fontSize: 18),
        ),
        leading: BackButton(
            color: Colors.grey,
            onPressed: () {
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => MainPage()));
            }),
        iconTheme: IconThemeData(color: Colors.black45),
      ),
      body: _body(),
    );
  }

  Widget _body() {
    return Container(
        color: Color(0xFFf1f2f6),
        child: Container(
            margin: EdgeInsets.all(25.0),
            child: Center(
                child: Column(
              children: <Widget>[
                Text('Actitivades contratadas'),
                _listContractedActivity(),
              ],
            ))));
  }

  Widget _listContractedActivity() {
    return Expanded(
        child: ListView.builder(
      itemCount: activitiesContracted == null ? 0 : activitiesContracted.length,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          margin: EdgeInsets.all(5.0),
          elevation: 10.0,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(1.0)),
          child: Column(
            children: <Widget>[
              Image(
                  image: NetworkImage(
                      'https://awakeningclaritynow.com/wp-content/uploads/Competitive-men-running-350x150.jpg'),
                  fit: BoxFit.contain),
              ListTile(
                title: Text("${activitiesContracted[index]["title"]}"),
                subtitle: Text("${activitiesContracted[index]["description"]}"),
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton.icon(
                    padding: EdgeInsets.all(0.8),
                      icon: Icon(
                        Icons.star,
                        color: Colors.yellow,
                      ),
                      label: Text('Calificar'),
                      onPressed: () {}),
                  FlatButton.icon(
                    padding: EdgeInsets.all(0.8),
                      icon:
                          Icon(FontAwesomeIcons.solidHeart, color: Colors.red),
                      label: Text('Favorita'),
                      onPressed: () {
                        showDialog(
                            context: context,
                            builder: (context) => AlertDialog(
                                  content:
                                      Text('Añadiste esta actividad a favoritos'
                                              '\n' +
                                          '\n'
                                              '${activitiesContracted[index]["title"]}.\n'),
                                  actions: <Widget>[
                                    FlatButton(
                                        child: Text('OK'),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) =>
                                                      FavoriteActivities()));
                                        }),
                                  ],
                                ));
                      }),
                  FlatButton.icon(
                    padding: EdgeInsets.all(0.8),
                      icon: Icon(Icons.share, color: Colors.black),
                      label: Text('Compartir'),
                      onPressed: () {
                        // texto para compartir actividad
                        Share.share(
                            'Disfruta de esta actividad como lo hice yo!');
                      }),
                ],
              ),
              Divider(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  FlatButton.icon(
                      icon: Icon(FontAwesomeIcons.check, color: Colors.black),
                      label: Text("Actividad Contratada: \$ ${activitiesContracted[index]["price"]}"),
                      onPressed: null),
                ],
              )
            ],
          ),
        );
      },
    ));
  }

  getActivitiesContracted() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();

    final value = sharedPreferences.get('token');
    var response = await http.get(
      'https://dev.fitgage.us/api/v1/service',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer ' + value
      },
    );

    var data = json.decode(response.body);
    setState(() {
      activitiesContracted = data;
    });
  }
}
