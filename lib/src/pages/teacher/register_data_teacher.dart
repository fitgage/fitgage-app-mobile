import 'package:flutter/material.dart';

import 'package:get_it/get_it.dart';
import 'package:fitgage/services/services.dart';
import 'package:fitgage/widgets/inputs.dart';
import 'package:address_search_field/address_search_field.dart';
import 'package:fitgage/src/pages/teacher/register_teacher.dart';
import 'package:fitgage/src/pages/teacher/services_teacher.dart';
import 'package:fitgage/providers/data_provider.dart';
import 'package:provider/provider.dart';

const API = 'https://dev.fitgage.us/api/v1/';

class RegisterDataTeacher extends StatefulWidget {
  @override
  _RegisterDataTeacher createState() => _RegisterDataTeacher();
}

class _RegisterDataTeacher extends State<RegisterDataTeacher> {
  bool _isLoading = false;
  final _formKey = new GlobalKey<FormState>();
  bool _blockCheck = false;
  AddressPoint _points;

  AppService get services => GetIt.I<AppService>();
  RegisterTeacher get check => GetIt.I<RegisterTeacher>();
  Inputs get inputs => GetIt.I<Inputs>();

  final TextEditingController addressSearchController =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Color(0xFFf1f2f6),
        centerTitle: true,
        title: Text('Datos Comerciales',
          style: TextStyle(color: Colors.black45),
        ),
        iconTheme: IconThemeData(
            color: Colors.black45
        ),
      ),
      body: Container(
        color: Color(0xFFf1f2f6),
        height: 1000,
        child: new Container(
          margin: EdgeInsets.all(35.0),
          child: SingleChildScrollView(
            child: _isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : new Form(
                    key: _formKey,
                    child: _formUI(),
                  ),
          ),
        ),
      ),
    );
  }

  Widget _formUI() {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Text(
              'Ingrese sus datos',
              style: TextStyle(
                fontSize: 25,
                letterSpacing: 1,
              ),
            ),
            Container(height: 40.0),
            inputs.inputBusinessName(),
            Container(height: 10.0),
            inputs.inputBusinessNumber(),
            Container(height: 10.0),
            inputs.inputIdentityNumber(),
            Container(height: 10.0),
            _inputAddressSearch(),
            Container(height: 10.0),
            inputs.inputPdto(),
            Container(height: 13.0),
            _checkBox(),
            Container(height: 13.0),
            _buildSuccess(),
          ],
        ),
      ),
    );
  }

  // NO tiene validación
  Widget _inputAddressSearch() {
    return AddressSearchField(
      country: "Argentina",
      noResultsText: 'No existen resultados',
      hintText: 'Busca tu dirección',
      controller: addressSearchController,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Domicilio',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      onDone: (AddressPoint point) {
        _points = point;
        print('points');
        print(_points);
        Navigator.of(context).pop();
      },
    );
  }

// mostrar mensaje
  Widget _checkBox() {
    return CheckboxListTile(
      controlAffinity: ListTileControlAffinity.leading,
      value: _blockCheck,
      onChanged: (value) {
        setState(() {
          _blockCheck = value;
        });
      },
      title: Text('Acepta Término y Condiciones',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black45, fontSize: 13)
      ),
    );
  }

  Widget _buildSuccess() {
    final dataInfo = Provider.of<DataInfo>(context);
    return Container(
        width: double.infinity,
        child: RaisedButton(
          elevation: 0.0,
          padding: EdgeInsets.all(17.0),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
          ),
          color: Colors.orange[500],
          child: Text(
            'Confirmar',
              style: TextStyle(
                  color: Color(0xFFf1f2f6),
                  letterSpacing: 1,
                  fontWeight: FontWeight.bold,
                  fontSize: 15
              )
          ),
          onPressed: () {
            if (_formKey.currentState.validate() == true) {
              dataInfo.checkOkData = false;
              if (_blockCheck == true) {
                setState(() async {
                  var res = await services.registerTeacher(
                      businessNameController.text,
                      cuitController.text,
                      dniController.text,
                      _points,
                      pdtoController.text,
                      _blockCheck);
                  if (res == 200 || res == 201) {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              title: Text('Muchas Gracias!'),
                              content: Text(
                                  'A partir de ahora podrás generar tus clases.'),
                              actions: <Widget>[
                                FlatButton(
                                    child: Text('OK'),
                                    onPressed: () {
                                      Navigator.of(context).pushAndRemoveUntil(
                                          MaterialPageRoute(
                                              builder: (BuildContext context) =>
                                                  ServicesTeacher()),
                                          (Route<dynamic> route) => false);
                                    })
                              ],
                            ));
                  }
                  if (res == 400) {
                    showDialog(
                        context: context,
                        builder: (context) => AlertDialog(
                              content: Text('Incorrect data'),
                              actions: <Widget>[
                                FlatButton(
                                    child: Text('OK'),
                                    onPressed: () {
                                      Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                RegisterDataTeacher()),
                                      );
                                    })
                              ],
                            ));
                  }
                });
              }
            }
          },
        ));
  }
}
