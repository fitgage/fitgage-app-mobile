import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/login.dart';
import 'package:fitgage/services/services.dart';
import 'package:get_it/get_it.dart';
import 'package:fitgage/widgets/inputs.dart';



class ResetPassword extends StatefulWidget {
  @override
  _ResetPassword createState() => _ResetPassword();
}

class _ResetPassword extends State<ResetPassword> {

  final _formKey = new GlobalKey<FormState>();

  AppService get services => GetIt.I<AppService>();
  Inputs get inputs => GetIt.I<Inputs>();
  final TextEditingController emailController = new TextEditingController();
  
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //resizeToAvoidBottomInset : false,
      body: Container(
        color: Color(0xFFf1f2f6),
        child: new Container(
            margin: EdgeInsets.all(30.0),
            child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : new Form(
                    key: _formKey,
                    child: formUI(),
                  )),
      ),
    );
  }

  Widget formUI() {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text(
              'Restablecer contraseña',
              style: TextStyle(
                  color: Color(0xFF35383d),
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1
              ),
            ),
            inputs.logo(),
            Container(height: 25.0),
            Text(
              'Escribe tu dirección de correo y te enviaremos',
              style: TextStyle(fontSize: 13),
            ),
            Text(
              'instrucciones para restablecer tu contraseña.',
              style: TextStyle(fontSize: 13),
            ),
            Container(height: 25.0),
            inputs.inputEmail(),
            Container(height: 25.0),
            _buildReset(),
            SizedBox(height: 15.0),
            _buildLoginBtn(),
          ],
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.spaceAround,
        ),
      ),
    );
  }

  Widget _buildReset() {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        elevation: 0.0,
        padding: EdgeInsets.all(17.0),
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0)
        ),
        color: Colors.orange[500],
        child: Text(
            'ENVIAR',
            style: TextStyle(
                color: Color(0xFFf1f2f6),
                letterSpacing: 1,
                fontWeight: FontWeight.bold,
                fontSize: 15
            )
        ),
        onPressed: () {
          if (_formKey.currentState.validate() == true) {
            setState(() {
              _isLoading = true;
              var testerEmail = 'tester@gmail.com';
              if (emailController.text == testerEmail){
                services.forgotPassword(emailController.text);
                  showDialog(context: context,
                  builder: (context) => AlertDialog(
                    content: Text('Se le ha enviado un mail para recuperar la contraseña'),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('OK'),
                        onPressed: (){
                          Navigator.push(
                            context, MaterialPageRoute(builder: 
                            (context) => Login()),
                          );
                        }) 
                    ],
                  ));

              }else {
                //Correo no existente
                showDialog(context: context,
                builder: (context) => AlertDialog(
                  content: Text('Email no existe.'),
                  actions: <Widget>[
                    FlatButton(child: Text('OK'),
                    onPressed: (){
                      Navigator.push(context, 
                      MaterialPageRoute(builder: (context) => ResetPassword()),);
                    })
                  ],
                ));
              }
            });
          }
        },
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      alignment: Alignment.center,
      child: FlatButton(
          //Action button
          onPressed: () => {Navigator.pushNamed(context, '/login')},
          padding: EdgeInsets.only(right: 10.0),
          child: Text('Volver a Iniciar Sesión')),
    );
  }
}
