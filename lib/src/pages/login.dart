import 'package:flutter/material.dart';

import 'package:fitgage/widgets/inputs.dart';
import 'package:fitgage/services/services.dart';
import 'package:get_it/get_it.dart';
import 'package:fitgage/src/pages/home.dart';
import 'package:fitgage/src/pages/registers_users.dart';

class Login extends StatefulWidget {
  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  bool _isLoading = false;
  final _formKey = new GlobalKey<FormState>();

  AppService get services => GetIt.I<AppService>();
  Inputs get inputs => GetIt.I<Inputs>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Color(0xFFf1f2f6),
        child: new Container(
            margin: EdgeInsets.all(30.0),
            child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : new Form(
                    key: _formKey,
                    child: formUI(),
                  )),
      ),
    );
  }

  Widget formUI() {
    return Center(
      child: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Text(
              'Bienvenido',
              style: TextStyle(
                  fontSize: 23,
                  color: Color(0xFF35383d),
                  fontWeight: FontWeight.bold,
                  letterSpacing: 1),
            ),
            inputs.logo(),
            SizedBox(height: 18.0),
            inputs.inputEmail(),
            Container(height: 30.0),
            inputs.inputPassword(),
            SizedBox(height: 13.0),
            _buildForgotPassword(),
            SizedBox(height: 25.0),
            _buildLogin(),
            SizedBox(height: 25.0),
            _buildRegisterBtn(),
          ],
        ),
      ),
    );
  }

  Widget _buildForgotPassword() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
          onPressed: () => {Navigator.pushNamed(context, '/reset_password')},
          padding: EdgeInsets.only(right: 0.0),
          child: Text(
            '¿Olvidaste tu contraseña?',
            style: TextStyle(color: Color(0xFF323338), fontSize: 12),
          )),
    );
  }

  Widget _buildLogin() {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        elevation: 0.0,
        padding: EdgeInsets.all(17.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
        color: Colors.orange[500],
        child: Text(
          'INICIAR SESION',
          style: TextStyle(
              color: Color(0xFFf1f2f6),
              letterSpacing: 1,
              fontWeight: FontWeight.bold,
              fontSize: 15),
        ),
        onPressed: loginUser,
      ),
    );
  }

  Future loginUser() async {
    if (_formKey.currentState.validate() == true) {
      setState(() {
        _isLoading = true;
      });

      var res = await services.signIn(
          usernameController.text, passwordController.text);

      if (res == 200 || res == 201) {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (BuildContext context) => MainPage()),
            (Route<dynamic> route) => false);
      } else {
        showDialog(
            context: context,
            builder: (context) => AlertDialog(
                  content: Text(
                      'No active account found with the given credentials'),
                  actions: <Widget>[
                    FlatButton(
                        child: Text('OK'),
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => Login()),
                          );
                        })
                  ],
                ));
      }
    }
  }

  Widget _buildRegisterBtn() {
    return Container(
        alignment: Alignment.centerRight,
        child: Center(
          child: FlatButton(
              //Action button
              onPressed: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => RegisterUsers()),
                    )
                  },
              child: Text(
                '¿No tienes una cuenta? Registrate ahora',
                style: TextStyle(color: Color(0xFF323338), fontSize: 13),
              )),
        ));
  }
}
