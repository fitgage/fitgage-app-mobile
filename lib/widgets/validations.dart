class Validations {
  String validateEmail(String value) {
    value = value.trim();
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.isEmpty) {
      return 'Can not be blank';
    } else if (!regExp.hasMatch(value)) {
      return 'Invalid email';
    } else {
      return null;
    }
  }

  String validatePassword(String value) {
    value = value.trim();
    if (value.length < 8) {
      return 'Password should contains more than 8 character';
    } else if (value.isEmpty) {
      return 'Can not be blank';
    }
    return null;
  }

  String validateName(String value) {
    value = value.trim();
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Can not be blank";
    } else if (!regExp.hasMatch(value)) {
      return "The name must be a-z y A-Z";
    } else if (value.length < 4) {
      return 'Name should contains more than 4 character';
    }
    return null;
  }

  String validateSurname(String value) {
    value = value.trim();
    String pattern = r'(^[a-zA-Z ]*$)';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Can not be blank";
    } else if (!regExp.hasMatch(value)) {
      return "The name must be a-z y A-Z";
    } else if (value.length < 4) {
      return 'Name should contains more than 4 character';
    }
    return null;
  }

  String validateRequired(String value){
    if(value.length == 0){
      return "Can not be blank";
    }
    return null;
  }
}

