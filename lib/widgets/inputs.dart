import 'package:flutter/material.dart';

import 'package:get_it/get_it.dart';
import 'package:fitgage/widgets/validations.dart';
import 'package:address_search_field/address_search_field.dart';

final TextEditingController nameController = new TextEditingController();
final TextEditingController surnameController = new TextEditingController();

final TextEditingController usernameController = new TextEditingController();
final TextEditingController passwordController = new TextEditingController();

final TextEditingController businessNameController =
    new TextEditingController();
final TextEditingController cuitController = new TextEditingController();
final TextEditingController dniController = new TextEditingController();
final TextEditingController pdtoController = new TextEditingController();

final TextEditingController titleController = new TextEditingController();
final TextEditingController descriptionController = new TextEditingController();
final TextEditingController priceController = new TextEditingController();
final TextEditingController observationsController = new TextEditingController();
final TextEditingController addressSearchController = new TextEditingController();

Validations get check => GetIt.I<Validations>();

class Inputs {
  Widget logo() {
    return Container(
        margin: const EdgeInsets.fromLTRB(20, 30, 20, 70),
        child: Image.asset('assets/logo.png',
            width: (260 * 0.6), height: (220 * 0.6)));
  }

  Widget inputName() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        icon: Icon(Icons.person),
        labelText: 'Nombre',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateName,
      controller: nameController,
    );
  }

  Widget inputSurname() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        icon: Icon(Icons.person),
        labelText: 'Apellido',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateSurname,
      controller: surnameController,
    );
  }

  Widget inputEmail() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        icon: Icon(Icons.email),
        labelText: 'Email',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateEmail,
      controller: usernameController,
    );
  }

  Widget inputPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8.0),
        icon: Icon(Icons.lock),
        labelText: 'Password',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validatePassword,
      controller: passwordController,
    );
  }

  Widget inputBusinessName() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Razón Social',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateRequired,
      controller: businessNameController,
    );
  }

  Widget inputBusinessNumber() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'CUIT',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateRequired,
      controller: cuitController,
    );
  }

  Widget inputIdentityNumber() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Nro Documento',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateRequired,
      controller: dniController,
    );
  }

  Widget inputPdto() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Piso / Dpto',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateRequired,
      controller: pdtoController,
    );
  }

  //Faltan todas las validaciones
  Widget inputTitle() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Título de tu actividad',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: null,
      controller: titleController,
    );
  }

  Widget inputDesciption() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
        labelText: 'Describe tu actividad',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      keyboardType: TextInputType.multiline,
      maxLines: 3,
      maxLength: 250,
      validator: null,
      controller: descriptionController,
    );
  }

  Widget inputPriceActivity() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        labelText: 'Precio de la Actividad',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      validator: check.validateRequired,
      controller: priceController,
    );
  }

  Widget inputObservations() {
    return TextFormField(
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
        labelText: 'Observaciones',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      keyboardType: TextInputType.multiline,
      maxLines: 3,
      maxLength: 150,
      validator: check.validateRequired,
      controller: observationsController,
    );
  }

  Widget inputAddressSearch(context) {
    // ignore: unused_local_variable
    AddressPoint _points;
    return AddressSearchField(
      country: "Argentina",
      noResultsText: 'No existen resultados',
      hintText: 'Busca tu dirección',
      controller: addressSearchController,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        contentPadding: EdgeInsets.only(left: 8),
        icon: Icon(
          Icons.location_on,
          color: Color(0xFF858688),
        ),
        labelText: 'Av. Angel Gallardo 1038, C1408 CABA, Argentina',
        labelStyle: TextStyle(fontWeight: FontWeight.bold, fontSize: 13),
      ),
      onDone: (AddressPoint point) {
        _points = point;
        print(_points);
        Navigator.of(context).pop();
      },
    );
  }
}
