import 'package:flutter/material.dart';

import 'package:fitgage/src/pages/login.dart';
import 'package:fitgage/src/pages/registers_users.dart';
import 'package:fitgage/src/pages/reset_password.dart';
import 'package:fitgage/src/pages/home.dart';
import 'package:fitgage/src/pages/register_data.dart';
import 'package:fitgage/src/pages/photography_users.dart';
import 'package:fitgage/src/pages/my_profile.dart';
import 'package:fitgage/src/pages/register_fitmedical_users.dart';

import 'package:fitgage/src/pages/teacher/register_teacher.dart';
import 'package:fitgage/src/pages/teacher/services_teacher.dart';
import 'package:fitgage/src/pages/teacher/register_services.dart';
import 'package:fitgage/src/pages/teacher/profile_teacher.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/buy_activity.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/card_pay_activity.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/finish_pay_activity.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/contracted_activities.dart';
import 'package:fitgage/src/pages/teacher/actions_activities/favorite_activities.dart';

Map<String, WidgetBuilder> getApplicationRoutes() {
  return <String, WidgetBuilder>{
    '/home'                                           : (BuildContext context) => MainPage(),
    '/login'                                          : (BuildContext context) => Login(),
    '/register_users'                                 : (BuildContext context) => RegisterUsers(),
    '/register_fitmedical_users'                      : (BuildContext context) => RegisterFitMedical(),
    '/reset_password'                                 : (BuildContext context) => ResetPassword(),
    '/register_data'                                  : (BuildContext context) => RegisterData(),
    '/my_profile'                                     : (BuildContext context) => MyProfile(),
    '/photography_users'                              : (BuildContext context) => UserPhotography(),
    'teacher/register_teacher'                        : (BuildContext context) => RegisterTeacher(),
    'teacher/services_teacher'                        : (BuildContext context) => ServicesTeacher(),
    'teacher/register_services'                       : (BuildContext context) => RegisterServices(),
    'teacher/profile_teacher'                         : (BuildContext context) => ProfileTeacher(),
    'teacher/actions_activities/buy_activity'         : (BuildContext context) => BuyActivity(),
    'teacher/actions_activities/card_pay_activity'    : (BuildContext context) => CardPayActivity(),
    'teacher/actions_activities/finish_pay_activity'  : (BuildContext context) => FinishPayActivity(),
    'teacher/actions_activities/contracted_activities': (BuildContext context) => ContractedActity(),
    'teacher/actions_activities/favorite_activities'  : (BuildContext context) => FavoriteActivities(),

    
  };
}
